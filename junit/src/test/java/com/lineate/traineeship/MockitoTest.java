package com.lineate.traineeship;

import org.junit.Test;
import org.mockito.Mockito;

public class MockitoTest {

    @Test
    public void testTest() {
        EntityRepository repository = Mockito.mock(EntityRepository.class);
        BaseEntityService entityService =
                new ServiceFactory().createEntityService(repository);

        User user = new User("", new Group(""));

        Entity entity = new Entity("e1");
        entity.setValue("");

        Mockito.when(repository.save(Mockito.any(Entity.class))).thenAnswer(a -> true);

        entityService.save(user, entity);
        entityService.save(user, entity);

        Mockito.verify(repository, Mockito.times(2)).save(entity);
    }
}
