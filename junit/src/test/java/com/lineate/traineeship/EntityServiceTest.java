package com.lineate.traineeship;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.Map;

public class EntityServiceTest {

    @Test
    public void testRead() {
        ServiceFactory serviceFactory = new ServiceFactory();
        UserService userService = serviceFactory.createUserService();
        EntityService entityService = serviceFactory.createEntityService();

        Group group1 = userService.createGroup("g1");
        User user1 = userService.createUser("u1");
        User user2 = userService.createUser("u2");
        userService.addUserToGroup(user1, group1);
        userService.addUserToGroup(user2, group1);

        Entity entity = new Entity("e1", "v1");
        entityService.save(user1, entity);
        entityService.grantPermission(entity, group1, Permission.read);

        Assert.assertEquals("v1", entityService.getByName(user1, "e1").getValue());
        Assert.assertEquals("v1", entityService.getByName(user2, "e1").getValue());

        entity.setValue("v2");
        Assert.assertTrue(entityService.save(user1, entity));
        Assert.assertEquals("v2", entityService.getByName(user1, "e1").getValue());
        Assert.assertEquals("v2", entityService.getByName(user2, "e1").getValue());

        entity.setValue("v3");
        Assert.assertFalse(entityService.save(user2, entity));
        Assert.assertEquals("v2", entityService.getByName(user1, "e1").getValue());
        Assert.assertEquals("v2", entityService.getByName(user2, "e1").getValue());

        Group group2 = userService.createGroup("g2");
        User user3 = userService.createUser("u3", group2);
        entity.setValue("v3");
        Assert.assertNull(entityService.getByName(user3, "e1"));
        Assert.assertFalse(entityService.save(user3, entity));

        Group group3 = userService.createGroup("g3");
        userService.addUserToGroup(user1, group3);
        userService.addUserToGroup(user3, group3);
        Entity entity2 = new Entity("e2", "v1");
        entityService.save(user1, entity2);
        entityService.grantPermission(entity2, group3, Permission.write);
        Assert.assertEquals("v1", entityService.getByName(user1, "e2").getValue());
        Assert.assertNull(entityService.getByName(user2, "e2"));
        Assert.assertEquals("v1", entityService.getByName(user3, "e2").getValue());
        entity2.setValue("v2");
        Assert.assertTrue(entityService.save(user3, entity2));
        Assert.assertEquals("v2", entityService.getByName(user1, "e2").getValue());
    }

    @Test
    public void testWrite() {
        ServiceFactory serviceFactory = new ServiceFactory();
        UserService userService = serviceFactory.createUserService();
        EntityService entityService = serviceFactory.createEntityService();

        Group group1 = userService.createGroup("g1");
        User user1 = userService.createUser("u1", group1);
        Entity entity = new Entity("e1", "v1");
        entityService.save(user1, entity);
        Assert.assertEquals("v1", entityService.getByName(user1, "e1").getValue());

        User user2 = userService.createUser("u2", group1);
        Assert.assertNull(entityService.getByName(user2, "e1"));

        entity.setValue("v2");
        Assert.assertTrue(entityService.save(user1, entity));
        Assert.assertEquals("v2", entityService.getByName(user1, "e1").getValue());
        Assert.assertNull(entityService.getByName(user2, "e1"));

        entity.setValue("v3");
        Assert.assertFalse(entityService.save(user2, entity));
        Assert.assertNotEquals("v3", entityService.getByName(user1, "e1").getValue());
        Assert.assertNull(entityService.getByName(user2, "e1"));

        entityService.grantPermission(entity, group1, Permission.write);

        entity.setValue("v3");
        Assert.assertTrue(entityService.save(user2, entity));
        Assert.assertEquals("v3", entityService.getByName(user1, "e1").getValue());
        Assert.assertNotNull(entityService.getByName(user2, "e1"));
    }

    @Test
    public void testDatabase() {
        ServiceFactory serviceFactory = new ServiceFactory();
        UserService userService = serviceFactory.createUserService();
        EntityRepository entityRepository = Mockito.mock(EntityRepository.class);
        BaseEntityService entityService = serviceFactory.createEntityService(entityRepository);
        Map<String, Entity> storedEntities = new HashMap<>();

        Mockito.when(entityRepository.save(Mockito.any(Entity.class))).thenAnswer(a -> {
            Entity entity = (Entity) a.getArguments()[0];
            storedEntities.put(entity.getName(), new Entity(entity));
            return true;
        });

        Mockito.when(entityRepository.getByName(Mockito.anyString())).thenAnswer(a -> {
            String name = (String) a.getArguments()[0];
            return storedEntities.get(name);
        });

        Group group1 = userService.createGroup("g1");
        User user1 = userService.createUser("u1", group1);
        Entity entity = new Entity("e1", "v1");
        entityService.save(user1, entity);
        entityService.grantPermission(entity, group1, Permission.read);

        Assert.assertEquals("v1", entityService.getByName(user1, "e1").getValue());

        User user2 = userService.createUser("u2", group1);
        Assert.assertEquals("v1", entityService.getByName(user2, "e1").getValue());

        entity.setValue("v2");
        Assert.assertTrue(entityService.save(user1, entity));
        Assert.assertEquals("v2", entityService.getByName(user1, "e1").getValue());
        Assert.assertEquals("v2", entityService.getByName(user2, "e1").getValue());

        entity.setValue("v3");
        Assert.assertFalse(entityService.save(user2, entity));
        Assert.assertEquals("v2", entityService.getByName(user1, "e1").getValue());
        Assert.assertEquals("v2", entityService.getByName(user2, "e1").getValue());
    }

}
