package com.lineate.traineeship;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

@RunWith(Parameterized.class)
public class EntityNameTest {

    @Parameterized.Parameters(name = "{index}: [{0}] -> {1}")
    public static Iterable<Object[]> names() {
        return Arrays.asList(new Object[][] {
                { "valid", true },
                { "invalid :(", false },
                { " ", false },
                { "  ", false },
                { "\t", true },
                { "1", true },
                { "0123456789012345678901234567890123", false },
                { "name!!11", true } });
    }

    @Parameterized.Parameter()
    public String input;

    @Parameterized.Parameter(1)
    public boolean expected;

    @Test
    public void testName() {
        ServiceFactory serviceFactory = new ServiceFactory();
        UserService userService = serviceFactory.createUserService();
        Group group = userService.createGroup("group");
        User user = userService.createUser("user", group);
        EntityService entityService = serviceFactory.createEntityService();
        final Entity entity = new Entity(input);
        Assert.assertEquals(expected, entityService.save(user, entity));
    }
}
